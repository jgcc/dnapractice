# DNA Práctica 

  

Pequeño ejercicio echo en php, laravel y postgre sql; que consiste en mostrar si una cadena de adn tiene o no mutación. 

## Instalación   

El proyecto utiliza la herramienta `sail` de laravel así que `docker` debe de estar en instalado después es solo de clonar el repositorio y seguir los sig. comandos. 

instalar las dependencias principales 

```sh 
php composer install 
``` 

descargar y configurar las imágenes de docker necesarias y correrá el servidor de laravel. 

```sh 
./vendor/bin/sail up 
``` 

en otra consola o terminal ejecutar las migraciones de base de datos. 

```sh 
./vendor/bin/sail php artisan migrate 
```  

y listo ahora se cuenta con dos puntos para hacer pruebas  

`POST - http://localhost/api/mutation` 
`GET - http://localhost/api/status` 