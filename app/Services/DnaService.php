<?php

namespace App\Services;

use Illuminate\Http\Exceptions\HttpResponseException;
use App\Models\Dna;

class DnaService
{
    private const regex_pattern = '/[(A)]{4}|[(C)]{4}|[(G)]{4}|[(T)]{4}/i';
    private const MUTATION_LENGTH = 4;

    private function isValidSize($dna) {
        $lenght = count($dna);
        $totalCount = strlen(implode('', $dna));
        if(($totalCount / $lenght) != $lenght) {
            throw new HttpResponseException(response()->json(['errors' => 'the input dna is not NxN.'], 422));
        }
    }

    public function hasMutation($dna) {
        $this->isValidSize($dna);
        $verticalSeqArray = $this->generateVerticalSeq($dna);
        $obliqueSeqArray = $this->generateObliqueSeq($dna);
        $seqArray = array_merge($dna, $verticalSeqArray, $obliqueSeqArray);        
        $result = false;
        foreach($seqArray as $seq) {
            if(preg_match(self::regex_pattern, $seq)) {
                $result = true;
                break;
            }
        }
        $dnaModel = new Dna;
        $dnaModel->sequence = implode(',', $dna);
        $dnaModel->hasMutation = $result;
        $dnaModel->save();
        return $result;
    }

    public function getStatus() {
        $count_mutation = Dna::where('hasMutation', '=', true)->count();
        $count_no_mutation = Dna::where('hasMutation', '=', false)->count();
        $ratio =  $count_no_mutation == 0 ? 0 : $count_mutation / $count_no_mutation;
        return [
            'count_mutation' => $count_mutation,
            'count_no_mutation' => $count_no_mutation,
            'ratio' => $ratio
        ];
    }

    private function generateVerticalSeq($dna) {
        $seqArray = array();
        $seqLen = strlen($dna[0]);
        for ($i=0; $i < $seqLen; $i++) {
            $str = '';
            for ($j=0; $j < count($dna); $j++) {                
                $str = $str . substr($dna[$j], $i, 1);
            }
            array_push($seqArray, $str);
        }
        return $seqArray;
    }

    private function generateObliqueSeq($dna) {
        $seqArray = array();        
        $seqLen = strlen($dna[0]);
        $offset = $seqLen - self::MUTATION_LENGTH;        
        for ($i=$offset, $l=0; $i >= 0; $i--, $l++) {
            $bottomSeqLeft = '';
            $topSeqLeft = '';
            $bottomSeqRight = '';
            $topSeqRight = '';
            for ($j=0, $r=$seqLen-1; $j < ($seqLen - $offset) + $l; $j++, $r--) {                
                $bottomSeqLeft .= substr($dna[$i + $j], $j, 1);
                $topSeqLeft .= substr($dna[$j], $i + $j, 1);
                $bottomSeqRight .= substr($dna[$i + $j], $r, 1);                
                $topSeqRight .= substr($dna[$j], $r - $i, 1);
            }            
            array_push($seqArray, $bottomSeqLeft);
            array_push($seqArray, $topSeqLeft);
            array_push($seqArray, $bottomSeqRight);
            array_push($seqArray, $topSeqRight);
        }        
        return $seqArray;
    }
}
