<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Dna extends Model
{
    use HasFactory;
    protected $fillable = ['sequence', 'hasMutation'];
}
