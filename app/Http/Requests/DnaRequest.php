<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Exceptions\HttpResponseException;

class DnaRequest extends FormRequest
{    
    public function authorize() {
        return true;
    }
 
    public function rules() {
        return [
            'dna' => 'required|array'
        ];
    }

    public function messages() {
        return [
            'dna.required' => ':attribute field is required',
            'dna.array' => ':attribute field must be an array'
        ];
    }

    protected function failedValidation(Validator $validator) {
        throw new HttpResponseException(response()->json(['errors' => $validator->errors()], 422));
    }

}
