<?php

namespace App\Http\Controllers;

use App\Http\Requests\DnaRequest;
use App\Services\DnaService;

class DnaController extends Controller
{
    private $dnaService;

    public function __construct(DnaService $service) {
        $this->dnaService = $service;
    }

    public function mutation(DnaRequest $request) {        
        $validData = $request->validated();        
        $result = $this->dnaService->hasMutation($validData['dna']);
        if($result) {
            return response()->json(['msg' => 'dna has mutation'], 403);
        }
        return response()->json(['msg' => 'dna has no mutation']);
    }    

    public function status() {
        $result = $this->dnaService->getStatus();
        return response()->json($result);
    }
}
